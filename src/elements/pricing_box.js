import React from 'react';
import {ListGroup, Button} from 'react-bootstrap';
import { Check2, X, CartFill} from 'react-bootstrap-icons';

const PricingBox = (props) => {
  const {title, price, borderLess, popular} = props;
  return (
      <div className={`pricing-box position-relative ${title} text-center rounded ${borderLess ? '' : 'border'} ${popular ? 'popular-tag' : '' }`}>
        <div className="py-5 px-3">
          {popular && <span className="text-uppercase position-absolute popular">Popular</span>}
          <h4>{title}</h4>
          <div className="h2 text-primary d-flex justify-content-center align-items-center">
            <span className="h4 text-secondary">$</span>{price}
          </div>
          <ListGroup as="ul" variant="border-0 check-list-group">
            <ListGroup.Item as="li" className="mt-3">
              <Check2 className="text-primary"/>
              Content development
            </ListGroup.Item>
            <ListGroup.Item as="li" className="mt-3">
              {title === 'Basic' ? <X className="text-muted"/> : <Check2 className="text-primary"/>}
              UX design
            </ListGroup.Item>
            <ListGroup.Item as="li" className="mt-3">
              {title === 'Basic' ? <X className="text-muted"/> : <Check2 className="text-primary"/>}
              Video production
            </ListGroup.Item>
            <ListGroup.Item as="li" className="mt-3">
              {title === 'Basic' || title === 'Standard' ? <X className="text-muted"/> : <Check2 className="text-primary"/>}
              App design
            </ListGroup.Item>
            <ListGroup.Item as="li" className="mt-3">
              {title === 'Basic' || title === 'Standard' || title === 'Professional' ? <X className="text-muted"/> : <Check2 className="text-primary"/>}
              Marketing campaign
            </ListGroup.Item>
          </ListGroup>
          <Button variant="primary font-weight-bold mt-4" size="lg">
            <CartFill className="mr-2" />
            Buy Now
          </Button>
        </div>
      </div>
  );
}

export default PricingBox;