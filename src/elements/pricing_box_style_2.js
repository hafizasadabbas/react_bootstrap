import React from 'react';
import {ListGroup, Button} from 'react-bootstrap';
import { Check2, X, CartFill} from 'react-bootstrap-icons';

const PricingBox = (props) => {
  const {title, price, borderLess, popular} = props;
  return (
      <div className={`pricing-box position-relative ${title} text-center`}>
        <div className={`py-4 px-3 rounded ${popular ? 'popular-tag' : '' } ${borderLess ? '' : 'border'}`}>
          <h4>{title}</h4>
          <div className="h2 text-primary d-flex justify-content-center align-items-center">
            <span className="h4 text-secondary">$</span>{price}<span className="h6 text-muted font-weight-normal ml-2 mt-4">/ monthly</span>
          </div>
          <Button variant="primary font-weight-bold mt-3" size="lg">
            <CartFill className="mr-2" />
            Buy Now
          </Button>
        </div>
        <div className="px-3">
          <ListGroup as="ul" variant="border-0 check-list-group" className={`py-5 ${borderLess ? '' : 'border border-top-0'}`}>
            <ListGroup.Item as="li">
              <Check2 className="text-primary"/>
              Content development
            </ListGroup.Item>
            <ListGroup.Item as="li" className="mt-3">
              {title === 'Basic' ? <X className="text-muted"/> : <Check2 className="text-primary"/>}
              UX design
            </ListGroup.Item>
            <ListGroup.Item as="li" className="mt-3">
              {title === 'Basic' ? <X className="text-muted"/> : <Check2 className="text-primary"/>}
              Video production
            </ListGroup.Item>
            <ListGroup.Item as="li" className="mt-3">
              {title === 'Basic' || title === 'Standard' || title === 'Optimal' ? <X className="text-muted"/> : <Check2 className="text-primary"/>}
              App design
            </ListGroup.Item>
            <ListGroup.Item as="li" className="mt-3">
              {title === 'Basic' || title === 'Standard' || title === 'Professional' ? <X className="text-muted"/> : <Check2 className="text-primary"/>}
              Marketing campaign
            </ListGroup.Item>
          </ListGroup>
        </div>
      </div>
  );
}

export default PricingBox;