import React from 'react';

const Logo = () => {
  return (
      <div className="logo">
        <svg xmlns="http://www.w3.org/2000/svg" width="100" height="50" viewBox="0 0 100 50">
          <text id="LOGO" transform="translate(0 39)" fill="#333" fontSize="40" fontFamily="Source Sans Pro" fontWeight="700"><tspan x="0" y="0">LOGO</tspan></text>
        </svg>
      </div>
  );
}

export default Logo;