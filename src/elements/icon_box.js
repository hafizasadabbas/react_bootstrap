import React from 'react';

const IconBox = (props) => {
  const {icon, title, learnMore, horizontal, textAlign, description, borderLess} = props;
  return (
      <div className={`icon-box ${borderLess ? '' : 'border'} ${textAlign ? textAlign : ''} ${horizontal ? 'horizontal' : ''}`}>
        <span className="icon">{icon}</span>
        <div className="py-4 px-3">
          <h4>{title}</h4>
          <p>{description}</p>
          {learnMore && <a className="text-primary font-weight-bold" href={learnMore}>Learn More</a>}
        </div>
      </div>
  );
}

export default IconBox;