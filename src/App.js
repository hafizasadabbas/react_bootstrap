import React from 'react';
import {Header_01, Header_02, Header_03, Header_04, Header_05, Header_06, Header_07,Features_01, Features_02, Features_03, Features_04, Features_05, Features_06, Features_07, Footer_01, Footer_02, Footer_03, Footer_04, Footer_05, CTA_01, CTA_02, CTA_03, Pricing_01, Pricing_02, News_01, News_02, News_03, Team_01, Team_02, Team_03, FAQ_01, FAQ_02, FAQ_03, Testimonials_01, Testimonials_02, Testimonials_03} from './components'
import './App.scss';

function App() {
  return (
    <>
      <Header_01 />
      <Header_02 />
      <Header_03 />
      <Header_04 />
      <Header_05 />
      <Header_06 />
      <Header_07 />
      <Features_01 />
      <Features_02 />
      <Features_03 />
      <Features_04 />
      <Features_05 />
      <Features_06 />
      <Features_07 />
      <Pricing_01 />
      <Pricing_02 />
      <News_01 />
      <News_02 />
      <News_03 />
      <Testimonials_01 />
      <Testimonials_02 />
      <Testimonials_03 />
      <Team_01 />
      <Team_02 />
      <Team_03 />
      <CTA_01 />
      <CTA_02 />
      <CTA_03 />
      <FAQ_01 />
      <FAQ_02 />
      <FAQ_03 />
      <Footer_01 />
      <Footer_02 />
      <Footer_03 />
      <Footer_04 />
      <Footer_05 />
    </>
  );
}

export default App;