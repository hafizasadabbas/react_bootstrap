import React from 'react';
import {Container, Row, Col, Card, ListGroup} from 'react-bootstrap';
import { FacebookIcon, InstagramIcon, TwitterIcon } from '../elements';
import placeholder_01 from '../images/team_placeholder_08.jpg';
import placeholder_02 from '../images/team_placeholder_09.jpg';
import placeholder_03 from '../images/team_placeholder_10.jpg';
import placeholder_04 from '../images/team_placeholder_11.jpg';

const Team = () => {
  return (
    <section className="py-5">
        <Container>
            <h2 className="text-center">Our Team</h2>
            <Row className="flex-column flex-sm-row mt-5">
                <Col className="col-sm-6 col-lg-3 mt-4 mt-lg-0">
                    <Card border="0">
                        <Card.Img variant="top" src={placeholder_01} className="rounded-circle" />
                        <Card.Body className="text-center px-0">
                            <Card.Title as="h4">Ezequiel Dengra</Card.Title>
                            <Card.Subtitle className="text-uppercase">
                                CEO
                            </Card.Subtitle>
                            <Card.Text className="text-muted">
                                Ac felis donec et odio pellentesque diam volutpat. Vel pharetra vel turpis.
                            </Card.Text>
                            <ListGroup variant="border-0 flex-row justify-content-center">
                                <ListGroup.Item as="a" className="mr-3">
                                    <div className="bg-primary rounded-circle d-flex p-6">
                                        <FacebookIcon width="20px" color="white"/>
                                    </div>
                                </ListGroup.Item>
                                <ListGroup.Item as="a" className="mr-3">
                                    <div className="bg-primary rounded-circle d-flex p-6">
                                        <TwitterIcon width="20px" color="white" />
                                    </div>
                                </ListGroup.Item>
                                <ListGroup.Item as="a">
                                    <div className="bg-primary rounded-circle d-flex p-6">
                                        <InstagramIcon width="20px" color="white"/>
                                    </div>
                                </ListGroup.Item>
                            </ListGroup>
                        </Card.Body>
                    </Card>
                </Col>
                <Col className="col-sm-6 col-lg-3 mt-4 mt-lg-0">
                    <Card border="0">
                        <Card.Img variant="top" src={placeholder_02} className="rounded-circle" />
                        <Card.Body className="text-center px-0">
                            <Card.Title as="h4">Justine Marshall</Card.Title>
                            <Card.Subtitle className="text-uppercase">
                                MANAGER
                            </Card.Subtitle>
                            <Card.Text className="text-muted">
                            Felis donec et odio pellentesque diam volutpat. Vel pharetra vel turpis nunc.
                            </Card.Text>
                            <ListGroup variant="border-0 flex-row justify-content-center">
                                <ListGroup.Item as="a" className="mr-3">
                                    <div className="bg-primary rounded-circle d-flex p-6">
                                        <FacebookIcon width="20px" color="white"/>
                                    </div>
                                </ListGroup.Item>
                                <ListGroup.Item as="a" className="mr-3">
                                    <div className="bg-primary rounded-circle d-flex p-6">
                                        <TwitterIcon width="20px" color="white" />
                                    </div>
                                </ListGroup.Item>
                                <ListGroup.Item as="a">
                                    <div className="bg-primary rounded-circle d-flex p-6">
                                        <InstagramIcon width="20px" color="white"/>
                                    </div>
                                </ListGroup.Item>
                            </ListGroup>
                        </Card.Body>
                    </Card>
                </Col>
                <Col className="col-sm-6 col-lg-3 mt-4 mt-lg-0">
                    <Card border="0">
                        <Card.Img variant="top" src={placeholder_03} className="rounded-circle" />
                        <Card.Body className="text-center px-0">
                            <Card.Title as="h4">Conan Matusov</Card.Title>
                            <Card.Subtitle className="text-uppercase">
                                DESIGNER
                            </Card.Subtitle>
                            <Card.Text className="text-muted">
                                Ac felis donec et odio pellentesque diam volutpat. Vel pharetra vel turpis.
                            </Card.Text>
                            <ListGroup variant="border-0 flex-row justify-content-center">
                                <ListGroup.Item as="a" className="mr-3">
                                    <div className="bg-primary rounded-circle d-flex p-6">
                                        <FacebookIcon width="20px" color="white"/>
                                    </div>
                                </ListGroup.Item>
                                <ListGroup.Item as="a" className="mr-3">
                                    <div className="bg-primary rounded-circle d-flex p-6">
                                        <TwitterIcon width="20px" color="white" />
                                    </div>
                                </ListGroup.Item>
                                <ListGroup.Item as="a">
                                    <div className="bg-primary rounded-circle d-flex p-6">
                                        <InstagramIcon width="20px" color="white"/>
                                    </div>
                                </ListGroup.Item>
                            </ListGroup>
                        </Card.Body>
                    </Card>
                </Col>
                <Col className="col-sm-6 col-lg-3 mt-4 mt-lg-0">
                    <Card border="0">
                        <Card.Img variant="top" src={placeholder_04} className="rounded-circle" />
                        <Card.Body className="text-center px-0">
                            <Card.Title as="h4">Clarke Gillebert</Card.Title>
                            <Card.Subtitle className="text-uppercase">
                                HR MANAGER
                            </Card.Subtitle>
                            <Card.Text className="text-muted">
                                Felis donec et odio pellentesque diam volutpat. Vel pharetra vel turpis nunc.
                            </Card.Text>
                            <ListGroup variant="border-0 flex-row justify-content-center">
                                <ListGroup.Item as="a" className="mr-3">
                                    <div className="bg-primary rounded-circle d-flex p-6">
                                        <FacebookIcon width="20px" color="white"/>
                                    </div>
                                </ListGroup.Item>
                                <ListGroup.Item as="a" className="mr-3">
                                    <div className="bg-primary rounded-circle d-flex p-6">
                                        <TwitterIcon width="20px" color="white" />
                                    </div>
                                </ListGroup.Item>
                                <ListGroup.Item as="a">
                                    <div className="bg-primary rounded-circle d-flex p-6">
                                        <InstagramIcon width="20px" color="white"/>
                                    </div>
                                </ListGroup.Item>
                            </ListGroup>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    </section>
  );
}

export default Team;