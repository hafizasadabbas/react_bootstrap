import React from 'react';
import {Container, Row, Col, Card, Image} from 'react-bootstrap';
import placeholder from '../images/testimonials_bg.jpg';

const Testimonials = () => {
  return (
    <section className="testimonials testimonial-container py-5 clearfix position-relative">
        <Image src={placeholder} className="float-right" fluid/>
        <Container className="position-absolute">
            <Row className="mt-5">
                <Col className="col-12 col-lg-10 bg-white border p-5">
                    <h2>Testimonials</h2>
                    <Row>
                        <Col className="col-6">
                            <Card border="0">
                                <Card.Body>
                                    <Card.Text>
                                        "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium."
                                    </Card.Text>
                                    <Card.Subtitle as="p" className="text-muted">
                                        – Marleah Eagleston, Google Inc.
                                    </Card.Subtitle>
                                </Card.Body>
                            </Card>
                        </Col>
                        <Col className="col-6">
                            <Card border="0">
                                <Card.Body>
                                    <Card.Text>
                                        "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium."
                                    </Card.Text>
                                    <Card.Subtitle as="p" className="text-muted">
                                        – Marleah Eagleston, Google Inc.
                                    </Card.Subtitle>
                                </Card.Body>
                            </Card>
                        </Col>
                        <Col className="col-6 mt-4">
                            <Card border="0">
                                <Card.Body>
                                    <Card.Text>
                                        "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium."
                                    </Card.Text>
                                    <Card.Subtitle as="p" className="text-muted">
                                        – Marleah Eagleston, Google Inc.
                                    </Card.Subtitle>
                                </Card.Body>
                            </Card>
                        </Col>
                        <Col className="col-6 mt-4">
                            <Card border="0">
                                <Card.Body>
                                    <Card.Text>
                                        "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium."
                                    </Card.Text>
                                    <Card.Subtitle as="p" className="text-muted">
                                        – Marleah Eagleston, Google Inc.
                                    </Card.Subtitle>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </Container>
    </section>
  );
}

export default Testimonials;