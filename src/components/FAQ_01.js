import React, {useContext} from 'react';
import {Container, Row, Col, Button, Card, Accordion, useAccordionToggle, Tabs, Tab } from 'react-bootstrap';
import AccordionContext from 'react-bootstrap/AccordionContext';

const FAQ = () => {
    var ContextAwareToggle = ({ children, eventKey, callback }) => {
        const currentEventKey = useContext(AccordionContext);
      
        const decoratedOnClick = useAccordionToggle(
          eventKey,
          () => callback && callback(eventKey),
        );
      
        const isCurrentEventKey = currentEventKey === eventKey;
      
        return (
          <button
            type="button"
            className={ isCurrentEventKey ? 'open' : 'close' }
            onClick={decoratedOnClick}
          >
            {children}
          </button>
        );
    }
  return (
    <>
      <section className="FAQ">
        <Container className="py-5">
            <h2 className="text-center">F.A.Q.</h2>
            <Row className="mt-5">
                <Col>
                    <Tabs defaultActiveKey="social" className="justify-content-center">
                        <Tab eventKey="social" title="Social">
                            <Row className="justify-content-between mt-5">
                                <Col className="col-12 col-md-4 col-lg-3">
                                    <Card border="0">
                                        <Card.Body className="p-0">
                                            <Card.Title as="h4">Social</Card.Title>
                                            <Card.Text>
                                                Suspendisse ultrices gravida dictum fusce ut placerat. Tortor vitae purus faucibus ornare suspendisse. Dignissim enim sit amet venenatis urna eget nunc.
                                            </Card.Text>
                                            <Button variant="primary" size="lg" className="font-weight-bold">Read More</Button>
                                        </Card.Body>
                                    </Card>
                                </Col>
                                <Col className="col-12 col-md-8 mt-5 mt-md-0">
                                    <Accordion defaultActiveKey="0">
                                        <Card>
                                            <Accordion.Toggle as={Card.Header} eventKey="0" className="text-uppercase font-weight-bold">
                                                VIDEO BACKGROUND
                                                <ContextAwareToggle eventKey="0"></ContextAwareToggle>
                                            </Accordion.Toggle>
                                            <Accordion.Collapse eventKey="0">
                                                <Card.Body className="text-muted pt-0">
                                                    Fermentum et sollicitudin ac orci phasellus egestas tellus. Magna fermentum iaculis eu non diam phasellus vestibulum lorem sed. Nascetur ridiculus mus mauris vitae ultricies leo. At volutpat diam ut venenatis tellus in metus. Nibh nisl condimentum id venenatis. Sagittis purus sit amet volutpat consequat.
                                                </Card.Body>
                                            </Accordion.Collapse>
                                        </Card>
                                        <Card>
                                            <Accordion.Toggle as={Card.Header} eventKey="1" className="text-uppercase font-weight-bold">
                                                MULTIPLE FLEXIBLE
                                                <ContextAwareToggle eventKey="1"></ContextAwareToggle>
                                            </Accordion.Toggle>
                                            <Accordion.Collapse eventKey="1">
                                                <Card.Body className="text-muted pt-0">
                                                    Fermentum et sollicitudin ac orci phasellus egestas tellus. Magna fermentum iaculis eu non diam phasellus vestibulum lorem sed. Nascetur ridiculus mus mauris vitae ultricies leo. At volutpat diam ut venenatis tellus in metus. Nibh nisl condimentum id venenatis. Sagittis purus sit amet volutpat consequat.
                                                </Card.Body>
                                            </Accordion.Collapse>
                                        </Card>
                                        <Card>
                                            <Accordion.Toggle as={Card.Header} eventKey="2" className="text-uppercase font-weight-bold">
                                                UPLOAD DIFFERENT
                                                <ContextAwareToggle eventKey="2"></ContextAwareToggle>
                                            </Accordion.Toggle>
                                            <Accordion.Collapse eventKey="2">
                                                <Card.Body className="text-muted pt-0">
                                                    Fermentum et sollicitudin ac orci phasellus egestas tellus. Magna fermentum iaculis eu non diam phasellus vestibulum lorem sed. Nascetur ridiculus mus mauris vitae ultricies leo. At volutpat diam ut venenatis tellus in metus. Nibh nisl condimentum id venenatis. Sagittis purus sit amet volutpat consequat.
                                                </Card.Body>
                                            </Accordion.Collapse>
                                        </Card>
                                    </Accordion>
                                </Col>
                            </Row>
                        </Tab>
                        <Tab eventKey="video" title="video">
                            
                        </Tab>
                        <Tab eventKey="animation" title="Animation">
                            
                        </Tab>
                        <Tab eventKey="text" title="Text">
                            
                        </Tab>
                        <Tab eventKey="pricing" title="Pricing">
                            
                        </Tab>
                    </Tabs>
                </Col>
            </Row>
            
        </Container>
      </section>
    </>
  );
}

export default FAQ;