import React from 'react';
import {Container, Row, Col} from 'react-bootstrap';
import {PricingBox} from '../elements';

const Pricing = () => {
  return (
    <section className="py-5">
        <Container>
            <h2 className="text-center">Our Plans</h2>
            <p className="text-center">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.</p>
            <Row className="flex-column flex-sm-row mt-5">
                <Col className="col-sm-6 col-lg-3 mt-4">
                    <PricingBox title="Basic" price="19"/>
                </Col>
                <Col className="col-sm-6 col-lg-3 mt-4">
                    <PricingBox title="Standard" price="49"/>
                </Col>
                <Col className="col-sm-6 col-lg-3 mt-4">
                    <PricingBox title="Professional" price="99" popular/>
                </Col>
                <Col className="col-sm-6 col-lg-3 mt-4">
                    <PricingBox title="Enterprise" price="199"/>
                </Col>
            </Row>
        </Container>
    </section>
  );
}

export default Pricing;