import React from 'react';
import {Container, Row, Col, Nav, Navbar, Button, Image, NavDropdown} from 'react-bootstrap';
import { Logo } from '../elements';
import placeHolder from '../images/565x565.jpg'

const Header = () => {
  return (
    <>
      <header className="header no-background vh-100">
        <Container>
            <Row>
                <Col>
                    <Navbar expand="xl" className="px-0">
                        <Navbar.Brand href="#home"><Logo /></Navbar.Brand>
                        <Navbar.Toggle aria-controls="basic-navbar-nav" />
                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="ml-auto">
                                <NavDropdown title="Themes">
                                    <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
                                    <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
                                    <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
                                    <NavDropdown.Divider />
                                    <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
                                </NavDropdown>
                                <Nav.Link href="#about" className="px-3">About</Nav.Link>
                                <Nav.Link href="#portfolio" className="px-3">Portfolio</Nav.Link>
                                <Nav.Link href="#careers" className="px-3">Careers</Nav.Link>
                                <Nav.Link href="#blog" className="px-3">Blog</Nav.Link>
                            </Nav>
                            <Button variant="outline-primary font-weight-bold btn-ghost ml-3">Contact Us</Button>
                        </Navbar.Collapse>
                    </Navbar>
                </Col>
            </Row>
            <Row>
                <Col className="col-lg-6 col-sm-12">
                    <div className="full-background-caption text-lg-left text-sm-center">
                        <h1 className="mb-3">
                            Enjoy our first quality services
                        </h1>
                        <p className="text-justify mb-3 mt-3 d-inline-block">
                            Nam gravida, nisl a ullamcorper varius, tellus enim lobortis lectus, sit amet hendrerit neque orci quis ante. Aenean quam ligula, viverra sed luctus id, laoreet id odio.
                        </p>
                        <div className="mt-3">
                            <Button variant="primary font-weight-bold mr-3" size="lg">Explore</Button>
                            <Button variant="outline-primary font-weight-bold btn-ghost ml-3" size="lg">Contact Us</Button>
                        </div>
                    </div>
                </Col>
                <Col className="pl-5 pt-5 col-lg-6 col-sm-12">
                    <Image src={placeHolder} fluid />
                </Col>
            </Row>
        </Container>
      </header>
    </>
  );
}

export default Header;