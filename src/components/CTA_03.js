import React from 'react';
import {Container, Row, Col, Button, Form, FormControl} from 'react-bootstrap';

const CTA = () => {
  return (
    <>
      <section className="CTA">
        <Container className="py-5">
            <Row className="justify-content-between">
                <Col className="col-12 col-md-6">
                  <div className="text-center bg-secondary text-white py-5 px-3 rounded">
                    <h6 className="font-weight-normal">We Help You!</h6>
                    <h2>Contact Us</h2>
                    <Form inline className="justify-content-center">
                      <FormControl type="text" placeholder="Your Email" size="lg" />
                      <Button variant="primary ml-sm-3 font-weight-bold btn-small" size="lg">Send</Button>
                    </Form>
                  </div>
                </Col>
                <Col className="col-12 col-md-6 mt-4 mt-md-0">
                  <div className="text-center bg-primary text-white py-5 px-3 rounded">
                    <h6 className="font-weight-normal">Let’s Talk!</h6>
                    <h2>Take Our Survey</h2>
                    <Button variant="light text-primary font-weight-bold bg-white" type="submit" size="lg">Start Now</Button>
                  </div>
                </Col>
            </Row>
        </Container>
      </section>
    </>
  );
}

export default CTA;