import React from 'react';
import {Container, Row, Col, ListGroup, Form, Image, FormControl, Button} from 'react-bootstrap';
import { FacebookIcon, InstagramIcon, TwitterIcon, YoutubeIcon } from '../elements';
import mapPlaceholder from '../images/map_placeholder_large.jpg'

const Footer = () => {
  return (
    <>
      <footer className="footer border-top">   
        <Container>
            <Row className="flex-column flex-sm-row justify-content-between align-items-center py-5">
                <Col className="col-sm-12 col-md-4 col-lg-6">
                  <Image src={mapPlaceholder} fluid />
                </Col>
                <Col className="col-sm-12 col-md-8 col-lg-6 col-xl-5 mt-4 mt-md-0">
                  <h2 className="text-secondary">Contact Us</h2>
                  <ListGroup as="ul" variant="border-0 icon-list-group">
                    <ListGroup.Item as="li" className="d-flex align-items-start">
                      <p className="text-muted">Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus.</p>
                    </ListGroup.Item>
                    <ListGroup.Item as="li" className="mt-3 d-flex align-items-start">
                      <p>354 King Street, Melbourne Victoria 5467 Australia</p>
                    </ListGroup.Item>
                    <ListGroup.Item as="li" className=" d-flex align-items-start">
                      <p>(0321) 645-798-021</p>
                    </ListGroup.Item>
                    <ListGroup.Item as="li" className="d-flex align-items-start">
                      <p>info@mail.com</p>
                    </ListGroup.Item>
                  </ListGroup>
                  <Form inline>
                    <FormControl type="text" placeholder="You're email" size="lg" />
                    <Button variant="primary ml-sm-2 font-weight-bold" size="lg">Subscribe</Button>
                  </Form>
                </Col>
            </Row>
        </Container>
        <Container fluid className="bg-light">
          <Row className="flex-column py-4">
              <Col className="text-center text-sm-right">
                <ListGroup variant="border-0 flex-row justify-content-center">
                    <ListGroup.Item as="a" className="mr-3">
                      <FacebookIcon width="20px" color="#FD5545" />
                    </ListGroup.Item>
                    <ListGroup.Item as="a" className="mr-3">
                      <TwitterIcon width="20px" color="#FD5545" />
                    </ListGroup.Item>
                    <ListGroup.Item as="a" className="mr-3">
                      <InstagramIcon width="20px" color="#FD5545" />
                    </ListGroup.Item>
                    <ListGroup.Item as="a">
                      <YoutubeIcon width="20px" color="#FD5545" />
                    </ListGroup.Item>
                  </ListGroup>
                </Col>
                <Col className="text-center mt-3">
                    <span className="text-muted">© Copyright 2018. All Rights Reserved.</span>
                </Col>
            </Row>
          </Container>
      </footer>
    </>
  );
}

export default Footer;