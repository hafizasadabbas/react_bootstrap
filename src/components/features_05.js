import React from 'react';
import {Container, Row, Col, Button} from 'react-bootstrap';
import {IconBox} from '../elements';
import { Heart, Search, Star, X } from 'react-bootstrap-icons';

const Features = () => {
  var description = 'Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit';
  return (
    <section className="py-5 features-filled-grid">
        <Container>
            <Row className="flex-column flex-sm-row justify-content-between">
                <Col className="col-sm-12 col-lg-3 d-flex flex-column justify-content-center text-center text-lg-left align-items-center align-items-lg-start">
                    <h2 className="h2-secondary-size">Reasons To Work With Us</h2>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes.</p>
                    <Button variant="outline-primary font-weight-bold btn-ghost" size="lg">Read More</Button>
                </Col>
                <Col className="col-sm-12 col-lg-8 mt-5 mt-lg-0">
                    <Row className="h-100 flex-column flex-sm-row">  
                        <Col className="col-sm-6 mb-3">
                            <IconBox icon={<Heart className="text-white" />} title="Powerful"  textAlign="left" description={description} />
                        </Col>
                        <Col className="col-sm-6 mb-3">
                            <IconBox icon={<Search className="text-white" />} title="Trusted" textAlign="left" description={description} />
                        </Col>
                        <Col className="col-sm-6 mt-3">
                            <IconBox icon={<Star className="text-white" />} title="Experience" textAlign="left" description={description} />
                        </Col>
                        <Col className="col-sm-6 mt-3">
                            <IconBox icon={<X className="text-white" />} title="Teamwork" textAlign="left" description={description} />
                        </Col>
                    </Row> 
                </Col>
            </Row>
        </Container>
    </section>
  );
}

export default Features;