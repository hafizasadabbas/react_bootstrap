import React from 'react';
import {Container, Row, Col, ListGroup, Navbar, Nav, Card, Image} from 'react-bootstrap';
import { Envelope} from 'react-bootstrap-icons';
import { Logo } from '../elements';
import placeholder_01 from '../images/placeholder_04.jpg';
import placeholder_02 from '../images/placeholder_05.jpg';
import placeholder_03 from '../images/placeholder_06.jpg';
import insta_placeholder_01 from '../images/insta_placeholder_01.jpg';
import insta_placeholder_02 from '../images/insta_placeholder_02.jpg';
import insta_placeholder_03 from '../images/insta_placeholder_03.jpg';
import Receiver from '../images/receiver.svg';
import Map from '../images/watch_map.svg';
const Footer = () => {
  return (
    <>
      <footer className="footer border-top">   
        <Container className="pt-5 pb-3">
            <Row className="flex-column flex-sm-row">
                <Col className="col-sm-6 col-lg-3">
                  <a href="/home" className="d-block mb-2">
                    <Logo />
                  </a>
                  <p className="text-muted">Enjoy high quality theme and professional support.</p>
                  <ListGroup as="ul" variant="border-0 icon-list-group">
                    <ListGroup.Item as="li" className="mt-3">
                      <Image src={Receiver} className="icon mr-2" />
                      (0321) 645-798-021
                    </ListGroup.Item>
                    <ListGroup.Item as="li" className="mt-3">
                      <Image src={Map} className="icon mr-2" />
                      Watch map
                    </ListGroup.Item>
                    <ListGroup.Item as="li" className="mt-3">
                      <Envelope className="text-primary icon mr-2" />
                      info@mail.com
                    </ListGroup.Item>
                  </ListGroup>
                </Col>
                <Col className="pt-2 col-sm-6 col-lg-3 mt-4 mt-sm-0">
                  <Navbar className="p-0 flex-column align-items-start bullet-list">
                    <Navbar.Text as="h6" className="text-uppercase text-secondary p-0">Menu</Navbar.Text>
                    <Nav className="flex-column">
                        <Nav.Link href="#home" className="pl-3 text-secondary">Home</Nav.Link>
                        <Nav.Link href="#about" className="pl-3 text-secondary">About</Nav.Link>
                        <Nav.Link href="#portfolio" className="pl-3 text-secondary">Portfolio</Nav.Link>
                        <Nav.Link href="#vacancy" className="pl-3 text-secondary">Vacancy</Nav.Link>
                        <Nav.Link href="#blog" className="pl-3 text-secondary">Blog</Nav.Link>
                        <Nav.Link href="#contactus" className="pl-3 text-secondary">Contact Us</Nav.Link>
                    </Nav>
                  </Navbar>
                </Col>
                <Col className="pt-2 col-sm-6 col-lg-3 mt-4 mt-sm-0">
                  <h6 className="text-uppercase text-secondary mb-4">BLOG</h6>
                  <ListGroup variant="border-0" as="ul">
                    <ListGroup.Item as="li" href="#">
                      <Card className="flex-row border-0" as="a">
                        <Card.Img variant="left" src={placeholder_01} />
                        <Card.Body className="py-0">
                            <Card.Text>
                              Unique site features
                            </Card.Text>
                            <Card.Text className="small">
                              May 28, 2018
                            </Card.Text>
                        </Card.Body>
                      </Card>
                    </ListGroup.Item>
                    <ListGroup.Item as="li" className="pt-2">
                      <Card className="flex-row border-0" as="a">
                        <Card.Img variant="left" src={placeholder_02} />
                        <Card.Body className="py-0">
                            <Card.Text>
                              Fast customization
                            </Card.Text>
                            <Card.Text className="small">
                              May 28, 2018
                            </Card.Text>
                        </Card.Body>
                      </Card>
                    </ListGroup.Item>
                    <ListGroup.Item as="li" className="pt-2">
                      <Card className="flex-row border-0" as="a">
                        <Card.Img variant="left" src={placeholder_03} />
                        <Card.Body className="py-0">
                            <Card.Text>
                              Professional support
                            </Card.Text>
                            <Card.Text className="small">
                              May 28, 2018
                            </Card.Text>
                        </Card.Body>
                      </Card>
                    </ListGroup.Item>
                  </ListGroup>
                </Col>
                <Col className="pt-2 col-sm-6 col-lg-3 mt-4 mt-sm-0">
                  <h6 className="text-uppercase text-secondary mb-4">INSTAGRAM</h6>
                  <ListGroup variant="border-0 flex-row flex-wrap">
                    <ListGroup.Item as="a" className="col-4">
                      <Image src={insta_placeholder_01} thumbnail />
                    </ListGroup.Item>
                    <ListGroup.Item as="a" className="col-4">
                      <Image src={insta_placeholder_02} thumbnail />
                    </ListGroup.Item>
                    <ListGroup.Item as="a" className="col-4">
                      <Image src={insta_placeholder_03} thumbnail />
                    </ListGroup.Item>
                    <ListGroup.Item as="a" className="col-4">
                      <Image src={insta_placeholder_01} thumbnail />
                    </ListGroup.Item>
                    <ListGroup.Item as="a" className="col-4">
                      <Image src={insta_placeholder_02} thumbnail />
                    </ListGroup.Item>
                    <ListGroup.Item as="a" className="col-4">
                      <Image src={insta_placeholder_03} thumbnail />
                    </ListGroup.Item>
                  </ListGroup>
                </Col>
            </Row>
            <hr/>
            <Row className="flex-column flex-sm-row">
                <Col className="text-center text-sm-left">
                    <span className="text-muted">© Copyright 2018</span>
                </Col>
                <Col className="text-center text-sm-right">
                  <span className="text-muted">All Rights Reserved</span>
                </Col>
            </Row>
        </Container>
      </footer>
    </>
  );
}

export default Footer;