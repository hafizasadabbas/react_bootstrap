import React, {useContext} from 'react';
import {Container, Row, Col, Card, Accordion, useAccordionToggle, Image } from 'react-bootstrap';
import AccordionContext from 'react-bootstrap/AccordionContext';
import faqPlaceholder from '../images/faq_placeholder.jpg';

const FAQ = () => {
    var ContextAwareToggle = ({ children, eventKey, callback }) => {
        const currentEventKey = useContext(AccordionContext);
      
        const decoratedOnClick = useAccordionToggle(
          eventKey,
          () => callback && callback(eventKey),
        );
      
        const isCurrentEventKey = currentEventKey === eventKey;
      
        return (
          <button
            type="button"
            className={ isCurrentEventKey ? 'open' : 'close' }
            onClick={decoratedOnClick}
          >
            {children}
          </button>
        );
    }
  return (
    <>
      <section className="FAQ">
        <Container className="py-5">
            <Row className="justify-content-between mt-5">
                <Col className="col-12 col-lg-6">
                    <h2>Build a theme as adaptable as possible</h2>
                    <Accordion defaultActiveKey="0" className="pr-lg-5 mt-4">
                        <Card>
                            <Accordion.Toggle as={Card.Header} eventKey="0" className="text-uppercase font-weight-bold">
                                VIDEO BACKGROUND
                                <ContextAwareToggle eventKey="0"></ContextAwareToggle>
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey="0">
                                <Card.Body className="text-muted pt-0">
                                    Fermentum et sollicitudin ac orci phasellus egestas tellus. Magna fermentum iaculis eu non diam phasellus vestibulum lorem sed. Nascetur ridiculus mus mauris vitae ultricies leo. At volutpat diam ut venenatis tellus in metus. Nibh nisl condimentum id venenatis. Sagittis purus sit amet volutpat consequat.
                                </Card.Body>
                            </Accordion.Collapse>
                        </Card>
                        <Card>
                            <Accordion.Toggle as={Card.Header} eventKey="1" className="text-uppercase font-weight-bold">
                                MULTIPLE FLEXIBLE
                                <ContextAwareToggle eventKey="1"></ContextAwareToggle>
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey="1">
                                <Card.Body className="text-muted pt-0">
                                    Fermentum et sollicitudin ac orci phasellus egestas tellus. Magna fermentum iaculis eu non diam phasellus vestibulum lorem sed. Nascetur ridiculus mus mauris vitae ultricies leo. At volutpat diam ut venenatis tellus in metus. Nibh nisl condimentum id venenatis. Sagittis purus sit amet volutpat consequat.
                                </Card.Body>
                            </Accordion.Collapse>
                        </Card>
                        <Card>
                            <Accordion.Toggle as={Card.Header} eventKey="2" className="text-uppercase font-weight-bold">
                                UPLOAD DIFFERENT
                                <ContextAwareToggle eventKey="2"></ContextAwareToggle>
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey="2">
                                <Card.Body className="text-muted pt-0">
                                    Fermentum et sollicitudin ac orci phasellus egestas tellus. Magna fermentum iaculis eu non diam phasellus vestibulum lorem sed. Nascetur ridiculus mus mauris vitae ultricies leo. At volutpat diam ut venenatis tellus in metus. Nibh nisl condimentum id venenatis. Sagittis purus sit amet volutpat consequat.
                                </Card.Body>
                            </Accordion.Collapse>
                        </Card>
                    </Accordion>
                </Col>
                <Col className="col-12 col-lg-6 mt-5 mt-lg-0">
                    <Image src={faqPlaceholder} fluid/>
                </Col>
            </Row>
        </Container>
      </section>
    </>
  );
}

export default FAQ;