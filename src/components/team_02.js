import React from 'react';
import {Container, Row, Col, Card, ListGroup} from 'react-bootstrap';
import { FacebookIcon, InstagramIcon, TwitterIcon, YoutubeIcon } from '../elements';
import placeholder_01 from '../images/team_placeholder_05.jpg';
import placeholder_02 from '../images/team_placeholder_06.jpg';
import placeholder_03 from '../images/team_placeholder_07.jpg';

const Team = () => {
  return (
    <section className="py-5 team-block">
        <Container>
            <h2 className="text-center">Our incredible team</h2>
            <p className="text-center">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium.</p>
            <Row className="flex-column flex-sm-row mt-5">
                <Col className="col-sm-6 col-lg-4 mt-4 mt-lg-0">
                    <Card border="0">
                        <Card.Img variant="top" src={placeholder_01} className="rounded" />
                        <Card.ImgOverlay className="d-flex align-items-end">
                            <Card.Body className="rounded bg-white text-center">
                                <Card.Title as="h4">Lucas Pacheco</Card.Title>
                                <Card.Text className="text-muted">
                                    Photograph
                                </Card.Text>
                                <ListGroup variant="border-0 flex-row justify-content-center">
                                    <ListGroup.Item as="a" className="mr-3">
                                        <FacebookIcon width="20px" color="#FD5545"/>
                                    </ListGroup.Item>
                                    <ListGroup.Item as="a" className="mr-3">
                                        <TwitterIcon width="20px" color="#FD5545" />
                                    </ListGroup.Item>
                                    <ListGroup.Item as="a" className="mr-3">
                                        <InstagramIcon width="20px" color="#FD5545"/>
                                    </ListGroup.Item>
                                    <ListGroup.Item as="a">
                                        <YoutubeIcon width="20px" color="#FD5545"/>
                                    </ListGroup.Item>
                                </ListGroup>
                            </Card.Body>
                        </Card.ImgOverlay>
                    </Card>
                </Col>
                <Col className="col-sm-6 col-lg-4 mt-4 mt-lg-0">
                    <Card border="0">
                        <Card.Img variant="top" src={placeholder_02} className="rounded" />
                        <Card.ImgOverlay className="d-flex align-items-end">
                            <Card.Body className="rounded bg-white text-center">
                                <Card.Title as="h4">Uzoma Buchi</Card.Title>
                                <Card.Text className="text-muted">
                                    Marketing Manager
                                </Card.Text>
                                <ListGroup variant="border-0 flex-row justify-content-center">
                                    <ListGroup.Item as="a" className="mr-3">
                                        <FacebookIcon width="20px" color="#FD5545"/>
                                    </ListGroup.Item>
                                    <ListGroup.Item as="a" className="mr-3">
                                        <TwitterIcon width="20px" color="#FD5545" />
                                    </ListGroup.Item>
                                    <ListGroup.Item as="a" className="mr-3">
                                        <InstagramIcon width="20px" color="#FD5545"/>
                                    </ListGroup.Item>
                                    <ListGroup.Item as="a">
                                        <YoutubeIcon width="20px" color="#FD5545"/>
                                    </ListGroup.Item>
                                </ListGroup>
                            </Card.Body>
                        </Card.ImgOverlay>
                    </Card>
                </Col>
                <Col className="col-sm-6 col-lg-4 mt-4 mt-lg-0">
                    <Card border="0">
                        <Card.Img variant="top" src={placeholder_03} className="rounded" />
                        <Card.ImgOverlay className="d-flex align-items-end">
                            <Card.Body className="rounded bg-white text-center">
                                <Card.Title as="h4">Saga Lindén</Card.Title>
                                <Card.Text className="text-muted">
                                    Web Developer
                                </Card.Text>
                                <ListGroup variant="border-0 flex-row justify-content-center">
                                    <ListGroup.Item as="a" className="mr-3">
                                        <FacebookIcon width="20px" color="#FD5545"/>
                                    </ListGroup.Item>
                                    <ListGroup.Item as="a" className="mr-3">
                                        <TwitterIcon width="20px" color="#FD5545" />
                                    </ListGroup.Item>
                                    <ListGroup.Item as="a" className="mr-3">
                                        <InstagramIcon width="20px" color="#FD5545"/>
                                    </ListGroup.Item>
                                    <ListGroup.Item as="a">
                                        <YoutubeIcon width="20px" color="#FD5545"/>
                                    </ListGroup.Item>
                                </ListGroup>
                            </Card.Body>
                        </Card.ImgOverlay>
                    </Card>
                </Col>
            </Row>
        </Container>
    </section>
  );
}

export default Team;