import React from 'react';
import {Container, Row, Col, Nav, Navbar, Button} from 'react-bootstrap';
import { PlayFill } from 'react-bootstrap-icons';
import { Logo } from '../elements';

const Header = () => {
  return (
    <>
      <header className="header vh-100 half-background">
        <Container>
            <Row>
                <Col>
                    <Navbar expand="xl" className="px-0">
                        <Navbar.Brand href="#home"><Logo /></Navbar.Brand>
                        <Navbar.Toggle aria-controls="basic-navbar-nav" />
                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="ml-auto">
                                <Nav.Link href="#about" className="px-3">About</Nav.Link>
                                <Nav.Link href="#portfolio" className="px-3">Portfolio</Nav.Link>
                                <Nav.Link href="#careers" className="px-3">Careers</Nav.Link>
                                <Nav.Link href="#blog" className="px-3">Blog</Nav.Link>
                            </Nav>
                        </Navbar.Collapse>
                    </Navbar>
                </Col>
            </Row>
            <Row className="align-items-center">
                <Col className="col-xl-5">
                    <div className="full-background-caption text-xl-left text-sm-center">
                        <h1 className="mb-3">
                        Quickly edit and customise your way
                        </h1>
                        <p className="text-justify mb-3 mt-3 d-inline-block">
                            Nam gravida, nisl a ullamcorper varius, tellus enim lobortis lectus, sit amet hendrerit neque orci quis ante. Aenean quam ligula, viverra sed luctus id, laoreet id odio.
                        </p>
                        <div className="mt-3">
                            <Button variant="primary rounded-circle mr-3 play-btn" size="lg">
                                <PlayFill size={30}/>
                            </Button>
                            <span className="text-primary font-weight-bold mr-2">Watch the video</span>
                            <span className="text-secondary ">1:34</span>
                        </div>
                    </div>
                </Col>
            </Row>
        </Container>
      </header>
    </>
  );
}

export default Header;