import React from 'react';
import {Container, Row, Col, Card, ListGroup} from 'react-bootstrap';
import { FacebookIcon, InstagramIcon, TwitterIcon } from '../elements';
import placeholder_01 from '../images/team_placeholder_01.jpg';
import placeholder_02 from '../images/team_placeholder_02.jpg';
import placeholder_03 from '../images/team_placeholder_03.jpg';
import placeholder_04 from '../images/team_placeholder_04.jpg';

const Team = () => {
  return (
    <section className="py-5 team-block">
        <Container>
            <Row>
                <Col>
                    <h2>Our team</h2>
                </Col>
                <Col className="d-flex align-items-center justify-content-end">
                    <a className="text-primary">View all</a>
                </Col>
            </Row>
            <Row className="flex-column flex-sm-row mt-5">
                <Col className="col-sm-6 col-lg-3 mt-4 mt-lg-0">
                    <Card border="0">
                        <div className="card-img-hover position-relative">
                            <Card.Img variant="top" src={placeholder_01} className="rounded"/>
                            <Card.ImgOverlay className="align-items-center justify-content-center rounded">
                                <ListGroup variant="border-0 flex-row">
                                    <ListGroup.Item as="a" className="mr-3">
                                        <FacebookIcon width="34px" color="white"/>
                                    </ListGroup.Item>
                                    <ListGroup.Item as="a" className="mr-3">
                                        <TwitterIcon width="34px" color="white" />
                                    </ListGroup.Item>
                                    <ListGroup.Item as="a">
                                        <InstagramIcon width="34px" color="white"/>
                                    </ListGroup.Item>
                                </ListGroup>
                            </Card.ImgOverlay>
                        </div>
                        <Card.Body className="px-0">
                            <Card.Title as="h4">Boris Ukhtomsky</Card.Title>
                            <Card.Text className="text-muted">
                                CEO
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
                <Col className="col-sm-6 col-lg-3 mt-4 mt-lg-0">
                    <Card border="0">
                        <div className="card-img-hover position-relative">
                            <Card.Img variant="top" src={placeholder_02} className="rounded" />
                            <Card.ImgOverlay className="align-items-center justify-content-center rounded">
                                <ListGroup variant="border-0 flex-row">
                                    <ListGroup.Item as="a" className="mr-3">
                                        <FacebookIcon width="34px" color="white"/>
                                    </ListGroup.Item>
                                    <ListGroup.Item as="a" className="mr-3">
                                        <TwitterIcon width="34px" color="white" />
                                    </ListGroup.Item>
                                    <ListGroup.Item as="a">
                                        <InstagramIcon width="34px" color="white"/>
                                    </ListGroup.Item>
                                </ListGroup>
                            </Card.ImgOverlay>
                        </div>
                        <Card.Body className="px-0">
                            <Card.Title as="h4">Dai Jiang</Card.Title>
                            <Card.Text className="text-muted">
                                Web designer
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
                <Col className="col-sm-6 col-lg-3 mt-4 mt-lg-0">
                    <Card border="0">
                        <div className="card-img-hover position-relative">
                            <Card.Img variant="top" src={placeholder_03} className="rounded" />
                            <Card.ImgOverlay className="align-items-center justify-content-center rounded">
                                <ListGroup variant="border-0 flex-row">
                                    <ListGroup.Item as="a" className="mr-3">
                                        <FacebookIcon width="34px" color="white"/>
                                    </ListGroup.Item>
                                    <ListGroup.Item as="a" className="mr-3">
                                        <TwitterIcon width="34px" color="white" />
                                    </ListGroup.Item>
                                    <ListGroup.Item as="a">
                                        <InstagramIcon width="34px" color="white"/>
                                    </ListGroup.Item>
                                </ListGroup>
                            </Card.ImgOverlay>
                        </div>
                        <Card.Body className="px-0">
                            <Card.Title as="h4">Saami Al Samad</Card.Title>
                            <Card.Text className="text-muted">
                                Photograper
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
                <Col className="col-sm-6 col-lg-3 mt-4 mt-lg-0">
                    <Card border="0">
                        <div className="card-img-hover position-relative">
                            <Card.Img variant="top" src={placeholder_04} className="rounded" />
                            <Card.ImgOverlay className="align-items-center justify-content-center rounded">
                                <ListGroup variant="border-0 flex-row">
                                    <ListGroup.Item as="a" className="mr-3">
                                        <FacebookIcon width="34px" color="white"/>
                                    </ListGroup.Item>
                                    <ListGroup.Item as="a" className="mr-3">
                                        <TwitterIcon width="34px" color="white" />
                                    </ListGroup.Item>
                                    <ListGroup.Item as="a">
                                        <InstagramIcon width="34px" color="white"/>
                                    </ListGroup.Item>
                                </ListGroup>
                            </Card.ImgOverlay>
                        </div>
                        <Card.Body className="px-0">
                            <Card.Title as="h4">Hector Mariano</Card.Title>
                            <Card.Text className="text-muted">
                                Marketing
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    </section>
  );
}

export default Team;