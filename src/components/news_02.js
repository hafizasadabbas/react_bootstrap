import React from 'react';
import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import placeholder_01 from '../images/news_placeholder_04.jpg';
import placeholder_02 from '../images/news_placeholder_05.jpg';
import placeholder_03 from '../images/news_placeholder_06.jpg';

const News = () => {
  return (
    <section className="py-5">
        <Container>
            <Row>
                <Col>
                    <h2>News</h2>
                </Col>
                <Col className="d-flex align-items-center justify-content-end">
                    <Button variant="primary font-weight-bold" size="lg">Read Our News</Button>
                </Col>
            </Row>
            <Row className="flex-column flex-sm-row mt-5">
                <Col className="col-sm-6 col-lg-4 mt-4 mt-lg-0">
                    <Card>
                        <Card.Img variant="top p-4" src={placeholder_01} />
                        <Card.Body className="text-center pt-0">
                            <Card.Title as="h4">Free hour</Card.Title>
                            <Card.Text className="small">
                                November 1st, 2017 <span className="text-primary ml-3">10 Comments</span>
                            </Card.Text>
                            <Card.Text className="text-muted">
                                The company is particularly proud of its headphones‘ noise canceling capabilities.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
                <Col className="col-sm-6 col-lg-4 mt-4 mt-lg-0">
                    <Card>
                        <Card.Img variant="top p-4" src={placeholder_02} />
                        <Card.Body className="text-center pt-0">
                            <Card.Title as="h4">Power of choice</Card.Title>
                            <Card.Text className="small">
                                November 1st, 2017 <span className="text-primary ml-3">5 Comments</span>
                            </Card.Text>
                            <Card.Text className="text-muted">
                                Company is particularly proud of its noise canceling capabilities..
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
                <Col className="col-sm-6 col-lg-4 mt-4 mt-lg-0">
                    <Card>
                        <Card.Img variant="top p-4" src={placeholder_03} />
                        <Card.Body className="text-center pt-0">
                            <Card.Title as="h4">What we like best</Card.Title>
                            <Card.Text className="small">
                                February 10th, 2019 <span className="text-primary ml-3">42 Comments</span>
                            </Card.Text>
                            <Card.Text className="text-muted">
                                Particularly proud of its headphones‘ noise canceling capabilities.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    </section>
  );
}

export default News;