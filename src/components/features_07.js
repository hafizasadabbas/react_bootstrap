import React from 'react';
import {Container, Row, Col} from 'react-bootstrap';
import {IconBox} from '../elements';

const Features = () => {
    var description = 'Aliquam non elit lacus. Praesent aliquet, ipsum id scelerisque convallis, mi ligula euismod.';
  return (
    <section className="py-5">
        <Container>
        <h2 className="text-center">Best services save the world</h2>
            <p className="text-center">Nam gravida, nisl a ullamcorper varius, tellus enim lobortis lectus, sit amet hendrerit neque orci quis ante. Aenean quam ligula, viverra sed luctus id, laoreet id odio. Pellentesque feugiat maximus placerat.</p>
            <Row className="flex-column flex-sm-row mt-5">
            <Col className="col-sm-6 col-lg-4 mt-4">
                    <IconBox icon="1" title="Amazing" description={description} textAlign="left" borderLess/>
                </Col>
                <Col className="col-sm-6 col-lg-4 mt-4">
                    <IconBox icon="2" title="Customized" description={description} textAlign="left" borderLess/>
                </Col>
                <Col className="col-sm-6 col-lg-4 mt-4">
                    <IconBox icon="3" title="Incredibly" description={description} textAlign="left" borderLess/>
                </Col>
            </Row>
        </Container>
    </section>
  );
}

export default Features;