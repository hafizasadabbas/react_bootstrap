import React from 'react';
import {Container, Row, Col, Card, Carousel} from 'react-bootstrap';
import {ChevronLeft, ChevronRight} from 'react-bootstrap-icons';
import placeholder_01 from '../images/testimonials_03.jpg';
import placeholder_02 from '../images/testimonials_02.jpg';
import placeholder_03 from '../images/testimonials_01.jpg';

const Testimonials = () => {
  return (
    <section className="testimonials py-5">
        <Container>
            <h2 className="text-center">What Our Clients Say</h2>
            <Row className="flex-column flex-sm-row mt-5 justify-content-center">
                <Col className="col-12 col-lg-10">
                    <Carousel indicators={false} prevIcon={<ChevronLeft aria-hidden="true" className="text-primary" />} nextIcon={<ChevronRight aria-hidden="true" className="text-primary" />}>
                        <Carousel.Item>
                            <Card border="0" className="flex-md-row">
                                <Card.Img variant="left" src={placeholder_01} className="rounded-circle" />
                                <Card.Body className="text-center text-sm-left">
                                    <Card.Text className="h4">
                                        "Nam gravida, nisl a ullamcorper varius, tellus enim lobortis lectus, sit amet hendrerit neque orci quis ante. Aenean quam ligula, viverra sed luctus id, laoreet id odio."
                                    </Card.Text>
                                    <Card.Title as="h6" className="text-uppercase mt-4">NAYARA DELAFUENTE</Card.Title>
                                    <Card.Subtitle as="p" className="small">
                                        Photographer
                                    </Card.Subtitle>
                                </Card.Body>
                            </Card>
                        </Carousel.Item>
                        <Carousel.Item>
                            <Card border="0" className="flex-md-row">
                                <Card.Img variant="left" src={placeholder_02} className="rounded-circle" />
                                <Card.Body className="text-center text-sm-left">
                                    <Card.Text className="h4">
                                        "Nam gravida, nisl a ullamcorper varius, tellus enim lobortis lectus, sit amet hendrerit neque orci quis ante. Aenean quam ligula, viverra sed luctus id, laoreet id odio."
                                    </Card.Text>
                                    <Card.Title as="h6" className="text-uppercase mt-4">Fatima Delgadillo</Card.Title>
                                    <Card.Subtitle as="p" className="small">
                                        Co-Founder
                                    </Card.Subtitle>
                                </Card.Body>
                            </Card>
                        </Carousel.Item>
                        <Carousel.Item>
                            <Card border="0" className="flex-md-row">
                                <Card.Img variant="left" src={placeholder_03} className="rounded-circle" />
                                <Card.Body className="text-center text-sm-left">
                                    <Card.Text className="h4">
                                        "Nam gravida, nisl a ullamcorper varius, tellus enim lobortis lectus, sit amet hendrerit neque orci quis ante. Aenean quam ligula, viverra sed luctus id, laoreet id odio."
                                    </Card.Text>
                                    <Card.Title as="h6" className="text-uppercase mt-4">NAYARA DELAFUENTE</Card.Title>
                                    <Card.Subtitle as="p" className="small">
                                        Photographer
                                    </Card.Subtitle>
                                </Card.Body>
                            </Card>
                        </Carousel.Item>
                    </Carousel>
                </Col>
            </Row>
        </Container>
    </section>
  );
}

export default Testimonials;