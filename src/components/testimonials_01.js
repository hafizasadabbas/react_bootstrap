import React from 'react';
import {Container, Row, Col, Card, Carousel} from 'react-bootstrap';
import placeholder_01 from '../images/testimonials_01.jpg';
import placeholder_02 from '../images/testimonials_02.jpg';

const Testimonials = () => {
  return (
    <section className="testimonials py-5">
        <Container>
            <h2 className="text-center">What Our Clients Say</h2>
            <Row className="flex-column flex-sm-row mt-5 justify-content-center">
                <Col className="col-12 col-lg-8">
                    <Carousel controls={false}>
                        <Carousel.Item>
                            <Card border="0">
                                <Card.Img variant="null" src={placeholder_01} className="rounded-circle" />
                                <Card.Body className="text-center px-0">
                                    <Card.Text>
                                        Ability proceeds from a fusion of skills, knowledge, understanding and imagination, consolidated by experience. Beauty is when you can appreciate yourself. When you love yourself, that’s when you’re most beautiful.
                                    </Card.Text>
                                    <Card.Title as="h6" className="text-uppercase">NAYARA DELAFUENTE</Card.Title>
                                    <Card.Subtitle as="p" className="small">
                                        Photographer
                                    </Card.Subtitle>
                                </Card.Body>
                            </Card>
                        </Carousel.Item>
                        <Carousel.Item>
                            <Card border="0">
                                <Card.Img variant="null" src={placeholder_02} className="rounded-circle" />
                                <Card.Body className="text-center px-0">
                                    <Card.Text>
                                        Ability proceeds from a fusion of skills, knowledge, understanding and imagination, consolidated by experience. Beauty is when you can appreciate yourself. When you love yourself, that’s when you’re most beautiful.
                                    </Card.Text>
                                    <Card.Title as="h6" className="text-uppercase">Fatima Delgadillo</Card.Title>
                                    <Card.Subtitle as="p" className="small">
                                        Co-Founder
                                    </Card.Subtitle>
                                </Card.Body>
                            </Card>
                        </Carousel.Item>
                        <Carousel.Item>
                            <Card border="0">
                                <Card.Img variant="null" src={placeholder_01} className="rounded-circle" />
                                <Card.Body className="text-center px-0">
                                    <Card.Text>
                                        Ability proceeds from a fusion of skills, knowledge, understanding and imagination, consolidated by experience. Beauty is when you can appreciate yourself. When you love yourself, that’s when you’re most beautiful.
                                    </Card.Text>
                                    <Card.Title as="h6" className="text-uppercase">NAYARA DELAFUENTE</Card.Title>
                                    <Card.Subtitle as="p" className="small">
                                        Photographer
                                    </Card.Subtitle>
                                </Card.Body>
                            </Card>
                        </Carousel.Item>
                    </Carousel>
                </Col>
            </Row>
        </Container>
    </section>
  );
}

export default Testimonials;