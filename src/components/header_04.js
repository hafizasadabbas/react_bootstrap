import React from 'react';
import {Container, Row, Col, Nav, Navbar, Button, Form, FormControl} from 'react-bootstrap';
import { Logo } from '../elements';

const Header = () => {
  return (
    <>
      <header className="header background-position-right vh-100">
        <Container>
            <Row>
                <Col>
                    <Navbar expand="xl" className="px-0">
                        <Navbar.Brand href="#home"><Logo /></Navbar.Brand>
                        <Navbar.Toggle aria-controls="basic-navbar-nav" />
                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="ml-auto">
                                <Nav.Link href="#about" className="px-3">About</Nav.Link>
                                <Nav.Link href="#portfolio" className="px-3">Portfolio</Nav.Link>
                                <Nav.Link href="#careers" className="px-3">Careers</Nav.Link>
                                <Nav.Link href="#blog" className="px-3">Blog</Nav.Link>
                            </Nav>
                        </Navbar.Collapse>
                    </Navbar>
                </Col>
            </Row>
            <Row className="align-items-center pt-5">
                <Col className="col-xl-7">
                    <div className="full-background-caption">
                        <h1 className="mb-3">
                            Design like a pro without Photoshop
                        </h1>
                        <p className="text-justify mb-3 mt-3 d-inline-block">
                            Nam gravida, nisl a ullamcorper varius, tellus enim lobortis lectus, sit amet hendrerit neque orci quis ante. Aenean quam ligula, viverra sed luctus id, laoreet id odio.
                        </p>
                        <Form inline className="mt-3">
                            <FormControl type="text" placeholder="Your Email" className="mr-sm-3" size="lg"/>
                            <Button variant="primary font-weight-bold" className="ml-sm-3" size="lg">Register</Button>
                        </Form>
                    </div>
                </Col>
            </Row>
        </Container>
      </header>
    </>
  );
}

export default Header;