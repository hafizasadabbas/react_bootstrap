import React from 'react';
import {Container, Row, Col, Card} from 'react-bootstrap';
import placeholder_01 from '../images/placeholder_01.jpg';
import placeholder_02 from '../images/placeholder_02.jpg';
import placeholder_03 from '../images/placeholder_03.jpg';

const Features = () => {
  return (
    <section className="py-5">
        <Container>
            <h2 className="text-center">We're motivated by the desire to achieve</h2>
            <p className="text-center">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.</p>
            <Row className="flex-column flex-sm-row mt-5">
                <Col className="col-sm-6 col-lg-4 mt-4 mt-lg-0">
                    <Card>
                        <Card.Img variant="top" src={placeholder_01} />
                        <Card.Body className="text-center">
                            <Card.Title as="h4">Modern Design</Card.Title>
                            <Card.Text>
                                Aliquam non elit lacus. Praesent aliquet, ipsum id scelerisque convallis, mi ligula euismod odio, vel dictum mi risus a mi.
                            </Card.Text>
                            <Card.Link href="#" className="font-weight-bold">Learn More</Card.Link>
                        </Card.Body>
                    </Card>
                </Col>
                <Col className="col-sm-6 col-lg-4 mt-4 mt-lg-0">
                    <Card>
                        <Card.Img variant="top" src={placeholder_02} />
                        <Card.Body className="text-center">
                            <Card.Title as="h4">Responsive Design</Card.Title>
                            <Card.Text>
                                Aliquam non elit lacus. Praesent aliquet, ipsum id scelerisque convallis, mi ligula euismod odio, vel dictum mi risus a mi.
                            </Card.Text>
                            <Card.Link href="#" className="font-weight-bold">Learn More</Card.Link>
                        </Card.Body>
                    </Card>
                </Col>
                <Col className="col-sm-6 col-lg-4 mt-4 mt-lg-0">
                    <Card>
                        <Card.Img variant="top" src={placeholder_03} />
                        <Card.Body className="text-center">
                            <Card.Title as="h4">Multi-purpose Use</Card.Title>
                            <Card.Text>
                                Aliquam non elit lacus. Praesent aliquet, ipsum id scelerisque convallis, mi ligula euismod odio, vel dictum mi risus a mi.
                            </Card.Text>
                            <Card.Link href="#" className="font-weight-bold">Learn More</Card.Link>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    </section>
  );
}

export default Features;