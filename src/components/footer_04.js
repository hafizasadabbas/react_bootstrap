import React from 'react';
import {Container, Row, Col, ListGroup, Image} from 'react-bootstrap';
import { Logo } from '../elements';
import mapPlaceholder from '../images/map_placeholder.jpg'

const Footer = () => {
  return (
    <>
      <footer className="footer border-top">   
        <Container className="pt-5 pb-3">
            <Row className="flex-column flex-sm-row">
                <Col className="col-sm-6 col-lg-3">
                  <a href="/home" className="d-block mb-2">
                    <Logo />
                  </a>
                  <p><span className="d-block">© Copyright 2018.</span>All Rights Reserved.</p>
                </Col>
                <Col className="pt-2 col-sm-6 col-lg-3">
                  <h6 className="text-uppercase text-secondary mb-4">ABOUT US</h6>
                  <p className="text-muted">Nullam ac augue. Orci varius natoque penatibus et magnis dis parturient montes, ridiculus mus. Aliquam enim leo, condimentum facilisis nulla sed, cursus arcu. Aliquam enim leo.</p>
                </Col>
                <Col className="pt-2 col-sm-6 col-lg-3 mt-4 mt-sm-0">
                  <h6 className="text-uppercase text-secondary mb-4">CONTACT US</h6>
                  <ListGroup as="ul" variant="border-0 icon-list-group">
                    <ListGroup.Item as="li" className="d-flex align-items-start">
                      <p>354 King Street, Melbourne Victoria 5467 Australia</p>
                    </ListGroup.Item>
                    <ListGroup.Item as="li" className=" d-flex align-items-start">
                      <p>(0321) 645-798-021</p>
                    </ListGroup.Item>
                    <ListGroup.Item as="li" className="d-flex align-items-start">
                      <p>info@mail.com</p>
                    </ListGroup.Item>
                  </ListGroup>
                </Col>
                <Col className="pt-2 col-sm-6 col-lg-3 mt-4 mt-sm-0">
                  <h6 className="text-uppercase text-secondary mb-4">WE ON MAP</h6>
                  <Image src={mapPlaceholder} fluid />
                </Col>
            </Row>
        </Container>
      </footer>
    </>
  );
}

export default Footer;