import React from 'react';
import {Container, Row, Col, Button} from 'react-bootstrap';

const CTA = () => {
  return (
    <>
      <section className="CTA">
        <Container className="py-5">
            <Row className="justify-content-between">
                <Col className="col-12 col-sm-6 text-center text-sm-left">
                   <h2>Want to work?</h2>
                   <p>Cras semper auctor aliquam. Sed porta sed lacus sit amet consectetur. Lorem ipsum dolor sit amet, consectetur adipiscin.</p>
                </Col>
                <Col className="col-12 col-sm-6 mt-4 mt-md-0 d-flex align-items-center justify-content-center justify-content-sm-end">
                    <Button variant="primary font-weight-bold" type="submit" size="lg">Contact Us</Button>
                </Col>
            </Row>
        </Container>
      </section>
    </>
  );
}

export default CTA;