import React from 'react';
import {Container, Row, Col} from 'react-bootstrap';
import {PricingBoxStyle2} from '../elements';

const Pricing = () => {
  return (
    <section className="py-5">
        <Container>
            <h2 className="text-center">Flexible plans</h2>
            <p className="text-center m-auto">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.</p>
            <Row className="flex-column flex-sm-row mt-5">
                <Col className="col-sm-6 col-lg-4 mt-4">
                    <PricingBoxStyle2 title="Basic" price="19"/>
                </Col>
                <Col className="col-sm-6 col-lg-4 mt-4">
                    <PricingBoxStyle2 title="Optimal" price="29" popular/>
                </Col>
                <Col className="col-sm-6 col-lg-4 mt-4">
                    <PricingBoxStyle2 title="Pro" price="99"/>
                </Col>
            </Row>
        </Container>
    </section>
  );
}

export default Pricing;