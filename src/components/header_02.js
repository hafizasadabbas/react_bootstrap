import React from 'react';
import {Container, Row, Col, Nav, Navbar, Button} from 'react-bootstrap';
import { Logo } from '../elements';

const Header = () => {
  return (
    <>
      <header className="header full-background vh-100">
        <Container>
            <Row>
                <Col>
                    <Navbar expand="xl" className="px-0">
                        <Navbar.Brand href="#home"><Logo /></Navbar.Brand>
                        <Navbar.Toggle aria-controls="basic-navbar-nav" />
                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="m-auto">
                                <Nav.Link href="#about" className="px-3">About</Nav.Link>
                                <Nav.Link href="#portfolio" className="px-3">Portfolio</Nav.Link>
                                <Nav.Link href="#careers" className="px-3">Careers</Nav.Link>
                                <Nav.Link href="#blog" className="px-3">Blog</Nav.Link>
                            </Nav>
                            <Button variant="outline-primary font-weight-bold btn-ghost ml-3">Contact Us</Button>
                        </Navbar.Collapse>
                    </Navbar>
                </Col>
            </Row>
            <Row>
                <Col className="text-center">
                    <div className="full-background-caption">
                        <h6 className="text-uppercase mb-3">Voluptate Velit</h6>
                        <h1 className="mb-5">
                            Designed to help you organize and manage your life
                        </h1>
                        <Button variant="primary font-weight-bold" size="lg">Read More</Button>
                    </div>
                </Col>
            </Row>
        </Container>
      </header>
    </>
  );
}

export default Header;