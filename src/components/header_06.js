import React from 'react';
import {Container, Col, Nav, Navbar, Button} from 'react-bootstrap';
import { Logo } from '../elements';

const Header = () => {
  return (
    <>
      <header className="header">
        <div className="w-100 bg-white">
            <Col>
                <Container>
                    <Navbar expand="xl" className="px-0">
                        <Navbar.Brand href="#home"><Logo /></Navbar.Brand>
                        <Navbar.Toggle aria-controls="basic-navbar-nav" />
                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="ml-auto">
                                <Nav.Link href="#about" className="px-3">About</Nav.Link>
                                <Nav.Link href="#portfolio" className="px-3">Portfolio</Nav.Link>
                                <Nav.Link href="#careers" className="px-3">Careers</Nav.Link>
                                <Nav.Link href="#blog" className="px-3">Blog</Nav.Link>
                            </Nav>
                        </Navbar.Collapse>
                    </Navbar>
                </Container>
            </Col>
        </div>
        <div className="background-hero w-100">
            <Col className="h-100">
                <Container className="position-relative h-100">
                    <div className="full-background-caption bg-white col-lg-5 position-absolute text-lg-left text-sm-center">
                        <h1 className="mb-3">
                            Fastest Growing Platform
                        </h1>
                        <p className="text-justify mb-3 mt-3 d-inline-block">
                            Quis hendrerit dolor magna eget est lorem ipsum. Amet justo donec enim diam vulputate ut.
                        </p>
                        <Button variant="primary font-weight-bold d-block m-auto m-lg-0" size="lg">Read More</Button>
                    </div>
                </Container>
            </Col>
        </div> 
      </header>
    </>
  );
}

export default Header;