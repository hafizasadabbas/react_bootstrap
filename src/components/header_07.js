import React from 'react';
import {Container, Row, Col, Nav, Navbar, Button} from 'react-bootstrap';
import { Logo, FacebookIcon, TwitterIcon, InstagramIcon } from '../elements';

const Header = () => {
  return (
    <>
      <header className="header background-pattern  vh-100 overflow-hidden">
        <Container>
            <Row>
                <Col>
                    <Navbar expand="xl" className="px-0">
                        <Navbar.Brand href="#home"><Logo /></Navbar.Brand>
                        <Navbar.Toggle aria-controls="basic-navbar-nav" />
                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="m-auto">
                                <Nav.Link href="#about" className="px-3">About</Nav.Link>
                                <Nav.Link href="#portfolio" className="px-3">Portfolio</Nav.Link>
                                <Nav.Link href="#careers" className="px-3">Careers</Nav.Link>
                                <Nav.Link href="#blog" className="px-3">Blog</Nav.Link>
                            </Nav>
                            <Nav className="">
                                <Nav.Link href="#facebook" className="px-2">
                                    <FacebookIcon width="20px" color="#FD5545" />                   
                                </Nav.Link>
                                <Nav.Link href="#facebook" className="px-2">
                                    <TwitterIcon width="20px" color="#FD5545" />                  
                                </Nav.Link>
                                <Nav.Link href="#facebook" className="px-2">
                                    <InstagramIcon width="20px" color="#FD5545" />                  
                                </Nav.Link>
                            </Nav>
                        </Navbar.Collapse>
                    </Navbar>
                </Col>
            </Row>
            <Row className="align-items-center pt-5">
                <Col className="col-lg-5">
                    <div className="full-background-caption text-sm-center text-lg-left">
                        <h1 className="mb-3">
                            Specially Designed Site for You
                        </h1>
                        <p className="text-justify mb-3 mt-3 d-inline-block">
                        Quis hendrerit dolor magna eget est lorem ipsum. Amet justo donec enim diam vulputate ut.
                        </p>
                        <div className="mt-3">
                            <Button variant="primary font-weight-bold mr-3" size="lg">Explore</Button>
                            <Button variant="outline-primary font-weight-bold btn-ghost ml-3" size="lg">Read More</Button>
                        </div>
                    </div>
                </Col>
            </Row>
        </Container>
      </header>
    </>
  );
}

export default Header;