import React from 'react';
import {Container, Row, Col, Button, Form, ListGroup} from 'react-bootstrap';
import { Paperclip } from 'react-bootstrap-icons';
import { FacebookIcon, InstagramIcon, TwitterIcon, YoutubeIcon } from '../elements';

const CTA = () => {
  return (
    <>
      <section className="CTA">
        <Container className="py-5">
            <Row className="justify-content-between">
                <Col className="col-12 col-md-4 col-lg-3">
                    <ListGroup as="ul" variant="border-0 icon-list-group h-100 justify-content-between">
                        <ListGroup.Item as="li">
                            <h6 className="text-uppercase text-secondary mb-2">ADDRESS</h6>
                            <p>346 Unknown Apt. 243, Somewhere 211909, Canada</p>
                        </ListGroup.Item>
                        <ListGroup.Item as="li">
                            <h6 className="text-uppercase text-secondary mb-2">CONTACT US</h6>
                            <p className="mb-1">info@mail.com</p>
                            <p className="mb-0">(0321) 645-798-021</p>
                        </ListGroup.Item>
                        <ListGroup.Item as="li" className="mt-3 mt-md-0">
                            <ListGroup variant="border-0 flex-row">
                                <ListGroup.Item as="a" className="mr-3">
                                    <FacebookIcon width="20px" color="#FD5545"/>
                                </ListGroup.Item>
                                <ListGroup.Item as="a" className="mr-3">
                                    <TwitterIcon width="20px" color="#FD5545" />
                                </ListGroup.Item>
                                <ListGroup.Item as="a" className="mr-3">
                                    <InstagramIcon width="20px" color="#FD5545"/>
                                </ListGroup.Item>
                                <ListGroup.Item as="a">
                                    <YoutubeIcon width="20px" color="#FD5545"/>
                                </ListGroup.Item>
                            </ListGroup>
                        </ListGroup.Item>
                    </ListGroup>
                </Col>
                <Col className="col-12 col-md-8 mt-5 mt-md-0">
                    <Form>
                        <Form.Row>
                            <Form.Group as={Col} controlId="formGridName">
                                <Form.Control type="name" placeholder="Name" size="lg"/>
                            </Form.Group>
                            <Form.Group as={Col} controlId="formGridEmail">
                                <Form.Control type="email" placeholder="Enter email" size="lg"/>
                            </Form.Group>
                        </Form.Row>
                        <Form.Group controlId="formGridPhoneNumber">
                            <Form.Control type="tel" placeholder="Phone number" size="lg" />
                        </Form.Group>
                        <Form.Group controlId="formGridtextarea">
                            <Form.Control as="textarea" placeholder="Message"  rows="3" size="lg" />
                        </Form.Group>
                        <Form.Row>
                            <Form.Group as={Col} className="mb-0">
                                <Form.File id="exampleFormControlFile1" label={<Paperclip className="text-primary"/>} custom/>
                            </Form.Group>
                            <Form.Group as={Col} className="d-flex justify-content-end mb-0">
                                <Button variant="primary font-weight-bold" type="submit" size="lg">
                                    Send Message
                                </Button>
                            </Form.Group>
                        </Form.Row>
                    </Form>
                </Col>
            </Row>
        </Container>
      </section>
    </>
  );
}

export default CTA;