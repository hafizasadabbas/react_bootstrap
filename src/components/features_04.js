import React from 'react';
import {Container, Row, Col, Image} from 'react-bootstrap';
import {IconBox} from '../elements';
import { Heart, Search, Star, X } from 'react-bootstrap-icons';
import headingBG from '../images/featuresHeading_bg.jpg'

const Features = () => {
    var description = 'Aliquam non elit lacus. Praesent aliquet, ipsum id scelerisque convallis, mi ligula euismod.';
  return (
    <section className="py-5 features-left-image-grid">
        <Container>
            <Row className="flex-column flex-sm-row">
                <Col className="col-sm-12 col-lg-4 position-relative d-flex justify-content-center align-items-center">
                    <Image className="h-100" src={headingBG} fluid />
                    <h2 className="position-absolute text-white">Features</h2>
                </Col>
                <Col className="col-sm-12 col-lg-8 mt-5 mt-lg-0">
                    <Row className="h-100 flex-column flex-sm-row">  
                        <Col className="col-sm-6 mb-3">
                            <IconBox icon={<Heart className="text-primary" />} title="Perfect Design"  horizontal description={description} />
                        </Col>
                        <Col className="col-sm-6 mb-3">
                            <IconBox icon={<Search className="text-primary" />} title="Web Development" horizontal description={description} />
                        </Col>
                        <Col className="col-sm-6 mt-3">
                            <IconBox icon={<Star className="text-primary" />} title="Social Media" horizontal description={description} />
                        </Col>
                        <Col className="col-sm-6 mt-3">
                            <IconBox icon={<X className="text-primary" />} title="Graphic Design" horizontal description={description} />
                        </Col>
                    </Row> 
                </Col>
            </Row>
        </Container>
    </section>
  );
}

export default Features;