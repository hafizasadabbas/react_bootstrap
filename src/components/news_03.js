import React from 'react';
import {Container, Row, Col, Card} from 'react-bootstrap';
import placeholder_01 from '../images/news_placeholder_07.jpg';
import placeholder_02 from '../images/news_placeholder_08.jpg';
import placeholder_03 from '../images/news_placeholder_09.jpg';
import placeholder_04 from '../images/news_placeholder_10.jpg';

const News = () => {
  return (
    <section className="py-5">
        <Container>
            <Row>
                <Col className="col-12 col-sm-6">
                    <h2>News</h2>
                </Col>
                <Col className="col-12 col-sm-6 d-flex align-items-center justify-content-end">
                    <p className="mb-0">Curabitur gravida arcu ac tortor dignissim convallis aenean et. Metus dictum at tempor commodo. Consectetur lorem donec massa sapien faucibus et molestie.</p>
                </Col>
            </Row>
            <Row className="flex-column flex-sm-row mt-5">
                <Col className="col-sm-12 col-lg-6 my-2">
                    <Card className="flex-sm-row border-0">
                        <Card.Img variant="left" src={placeholder_01} className="col-sm-6 px-0" />
                        <Card.Body className="col-sm-6 text-center text-sm-left">
                            <Card.Title as="h4" className="text-truncate">Enjoy a pleasure</Card.Title>
                            <Card.Text className="text-muted">
                                Donec et odio pellentesque diam volutpat commodo sed. Enim nunc faucibus a pellentesque sit amet porttitor. Posuere sollicitudin.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
                <Col className="col-sm-12 col-lg-6 mt-4 my-2">
                    <Card className="flex-sm-row border-0">
                        <Card.Img variant="left" src={placeholder_02} className="col-sm-6 px-0" />
                        <Card.Body className="col-sm-6 text-center text-sm-left">
                            <Card.Title as="h4" className="text-truncate">Resultant pleasure</Card.Title>
                            <Card.Text className="text-muted">
                                Et odio pellentesque diam volutpat commodo sed. Enim nunc faucibus a pellentesque sit amet porttitor. Posuere sollicitudin.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
                <Col className="col-sm-12 col-lg-6 mt-4 my-2">
                    <Card className="flex-sm-row border-0">
                        <Card.Img variant="left" src={placeholder_03} className="col-sm-6 px-0" />
                        <Card.Body className="col-sm-6 text-center text-sm-left">
                            <Card.Title as="h4" className="text-truncate">Always holds in matters</Card.Title>
                            <Card.Text className="text-muted">
                                Donec et odio pellentesque diam volutpat commodo sed. Enim nunc faucibus a pellentesque sit amet porttitor. Posuere sollicitudin.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
                <Col className="col-sm-12 col-lg-6 mt-4 my-2">
                    <Card className="flex-sm-row border-0">
                        <Card.Img variant="left" src={placeholder_04} className="col-sm-6 px-0" />
                        <Card.Body className="col-sm-6 text-center text-sm-left">
                            <Card.Title as="h4" className="text-truncate">Righteous indignation</Card.Title>
                            <Card.Text className="text-muted">
                                Et odio pellentesque diam volutpat commodo sed. Enim nunc faucibus a pellentesque sit amet porttitor. Posuere sollicitudin.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    </section>
  );
}

export default News;