import Header_01 from './header_01';
import Header_02 from './header_02';
import Header_03 from './header_03';
import Header_04 from './header_04';
import Header_05 from './header_05';
import Header_06 from './header_06';
import Header_07 from './header_07';
import Features_01 from './features_01';
import Features_02 from './features_02';
import Features_03 from './features_03';
import Features_04 from './features_04';
import Features_05 from './features_05';
import Features_06 from './features_06';
import Features_07 from './features_07';
import Footer_01 from './footer_01';
import Footer_02 from './footer_02';
import Footer_03 from './footer_03';
import Footer_04 from './footer_04';
import Footer_05 from './footer_05';
import CTA_01 from './CTA_01';
import CTA_02 from './CTA_02';
import CTA_03 from './CTA_03';
import Pricing_01 from './pricing_01';
import Pricing_02 from './pricing_02';
import News_01 from './news_01';
import News_02 from './news_02';
import News_03 from './news_03';
import Team_01 from './team_01';
import Team_02 from './team_02';
import Team_03 from './team_03';
import FAQ_01 from './FAQ_01';
import FAQ_02 from './FAQ_02';
import FAQ_03 from './FAQ_03';
import Testimonials_01 from './testimonials_01';
import Testimonials_02 from './testimonials_02';
import Testimonials_03 from './testimonials_03';

export {
    Header_01,
    Header_02,
    Header_03,
    Header_04,
    Header_05,
    Header_06,
    Header_07,
    Features_01,
    Features_02,
    Features_03,
    Features_04,
    Features_05,
    Features_06,
    Features_07,
    Footer_01,
    Footer_02,
    Footer_03,
    Footer_04,
    Footer_05,
    CTA_01,
    CTA_02,
    CTA_03,
    Pricing_01,
    Pricing_02,
    News_01,
    News_02,
    News_03,
    Team_01,
    Team_02,
    Team_03,
    FAQ_01,
    FAQ_02,
    FAQ_03,
    Testimonials_01,
    Testimonials_02,
    Testimonials_03
};