import React, {useContext} from 'react';
import {Container, Row, Col, Card, Accordion, useAccordionToggle, Button } from 'react-bootstrap';
import AccordionContext from 'react-bootstrap/AccordionContext';

const FAQ = () => {
    var ContextAwareToggle = ({ children, eventKey, callback }) => {
        const currentEventKey = useContext(AccordionContext);
      
        const decoratedOnClick = useAccordionToggle(
          eventKey,
          () => callback && callback(eventKey),
        );
      
        const isCurrentEventKey = currentEventKey === eventKey;
      
        return (
          <button
            type="button"
            className={ isCurrentEventKey ? 'open' : 'close' }
            onClick={decoratedOnClick}
          >
            {children}
          </button>
        );
    }
  return (
    <>
      <section className="FAQ bottom-border-only">
        <Container className="py-5">
            <h6 className="text-center text-uppercase mb-3">KNOW MORE</h6>
            <h2 className="text-center">Frequently Asked Questions</h2>
            <Row className="justify-content-center mt-5">
                <Col className="col-12 col-lg-6">
                    <Accordion defaultActiveKey="0">
                        <Card>
                            <Accordion.Toggle as={Card.Header} eventKey="0" className="font-weight-bold">
                                <h4>Et magnis dis parturient montes?</h4>
                                <ContextAwareToggle eventKey="0"></ContextAwareToggle>
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey="0">
                                <Card.Body className="pt-0">
                                    Ac placerat vestibulum lectus mauris ultrices eros. Tincidunt eget nullam non nisi est sit amet facilisis magna. Id faucibus nisl tincidunt eget nullam non.
                                </Card.Body>
                            </Accordion.Collapse>
                        </Card>
                        <Card>
                            <Accordion.Toggle as={Card.Header} eventKey="1" className="font-weight-bold">
                                <h4>Et magnis dis parturient montes?</h4>
                                <ContextAwareToggle eventKey="1"></ContextAwareToggle>
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey="1">
                                <Card.Body className="pt-0">
                                Ac placerat vestibulum lectus mauris ultrices eros. Tincidunt eget nullam non nisi est sit amet facilisis magna. Id faucibus nisl tincidunt eget nullam non.
                                </Card.Body>
                            </Accordion.Collapse>
                        </Card>
                        <Card>
                            <Accordion.Toggle as={Card.Header} eventKey="2" className="font-weight-bold">
                                <h4>Ac felis donec et odio pellentesque?</h4>
                                <ContextAwareToggle eventKey="2"></ContextAwareToggle>
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey="2">
                                <Card.Body className="pt-0">
                                Ac placerat vestibulum lectus mauris ultrices eros. Tincidunt eget nullam non nisi est sit amet facilisis magna. Id faucibus nisl tincidunt eget nullam non.
                                </Card.Body>
                            </Accordion.Collapse>
                        </Card>
                        <Card>
                            <Accordion.Toggle as={Card.Header} eventKey="3" className="font-weight-bold">
                                <h4>Vel pharetra vel turpis?</h4>
                                <ContextAwareToggle eventKey="3"></ContextAwareToggle>
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey="3">
                                <Card.Body className="pt-0">
                                Ac placerat vestibulum lectus mauris ultrices eros. Tincidunt eget nullam non nisi est sit amet facilisis magna. Id faucibus nisl tincidunt eget nullam non.
                                </Card.Body>
                            </Accordion.Collapse>
                        </Card>
                        <Card>
                            <Accordion.Toggle as={Card.Header} eventKey="4" className="font-weight-bold">
                                <h4>Odio ut sem nulla pharetra?</h4>
                                <ContextAwareToggle eventKey="4"></ContextAwareToggle>
                            </Accordion.Toggle>
                            <Accordion.Collapse eventKey="4">
                                <Card.Body className="pt-0">
                                Ac placerat vestibulum lectus mauris ultrices eros. Tincidunt eget nullam non nisi est sit amet facilisis magna. Id faucibus nisl tincidunt eget nullam non.
                                </Card.Body>
                            </Accordion.Collapse>
                        </Card>
                    </Accordion>
                </Col>
            </Row>
            <Row className="mt-5">
                <Col className="d-flex justify-content-center">
                    <Button variant="primary" size="lg" className="font-weight-bold">Contact Support</Button>
                </Col>
            </Row>
        </Container>
      </section>
    </>
  );
}

export default FAQ;