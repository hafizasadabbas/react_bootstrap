import React from 'react';
import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import placeholder_01 from '../images/news_placeholder_01.jpg';
import placeholder_02 from '../images/news_placeholder_02.jpg';
import placeholder_03 from '../images/news_placeholder_03.jpg';

const News = () => {
  return (
    <section className="py-5">
        <Container>
            <h2 className="text-center">Our News</h2>
            <p className="text-center">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.</p>
            <Row className="flex-column flex-sm-row mt-5">
                <Col className="col-sm-6 col-lg-4 mt-4">
                    <Card border="0">
                        <Card.Img variant="top" src={placeholder_01} />
                        <Card.Body className="text-center">
                            <Card.Title as="h4">New Surface Headphones</Card.Title>
                            <Card.Text className="small">
                                November 1st, 2017
                            </Card.Text>
                            <Card.Text>
                                The company is particularly proud of its headphones‘ noise canceling capabilities.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
                <Col className="col-sm-6 col-lg-4 mt-4">
                    <Card border="0">
                        <Card.Img variant="top" src={placeholder_02} />
                        <Card.Body className="text-center">
                            <Card.Title as="h4">Blogging around the world</Card.Title>
                            <Card.Text className="small">
                                November 1st, 2017
                            </Card.Text>
                            <Card.Text>
                                Company is particularly proud of its noise canceling capabilities..
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
                <Col className="col-sm-6 col-lg-4 mt-4">
                    <Card border="0">
                        <Card.Img variant="top" src={placeholder_03} />
                        <Card.Body className="text-center">
                            <Card.Title as="h4">Dynamic Frameworks</Card.Title>
                            <Card.Text className="small">
                                November 1st, 2017
                            </Card.Text>
                            <Card.Text>
                                Particularly proud of its headphones‘ noise canceling capabilities.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
            <Row className="mt-5">
                <Col className="text-center">
                    <Button variant="primary font-weight-bold" size="lg">All News</Button>
                </Col>
            </Row>
        </Container>
    </section>
  );
}

export default News;