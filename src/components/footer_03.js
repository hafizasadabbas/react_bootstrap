import React from 'react';
import {Container, Row, Col, ListGroup, Form, Image, FormControl, Button} from 'react-bootstrap';
import { FacebookIcon, InstagramIcon, TwitterIcon, YoutubeIcon } from '../elements';
import insta_placeholder_01 from '../images/insta_placeholder_07.jpg';
import insta_placeholder_02 from '../images/insta_placeholder_08.jpg';
import insta_placeholder_03 from '../images/insta_placeholder_09.jpg';

const Footer = () => {
  return (
    <>
      <footer className="footer border-top">   
        <Container className="pt-5 pb-3">
            <Row className="flex-column flex-sm-row">
                <Col className="col-sm-6 col-lg-3">
                  <h6 className="text-uppercase text-secondary mb-4">ABOUT</h6>
                  <p>Nullam ac augue. Orci varius natoque penatibus et magnis dis parturient montes, ridiculus mus. Aliquam enim leo, condimentum facilisis nulla sed, cursus arcu. Aliquam enim leo.</p>
                </Col>
                <Col className="col-sm-6 col-lg-3 mt-4 mt-sm-0">
                  <h6 className="text-uppercase text-secondary mb-4">TWEET’S</h6>
                  <ListGroup as="ul" variant="border-0 icon-list-group icon-bullets">
                    <ListGroup.Item as="li" className="d-flex align-items-start">
                      <TwitterIcon className="mr-3" width="20px" color="#FD5545" />
                      <p>Aliquam enim leo, condimentum facilisis nulla sed, cursus arcu. Aliquam enim leo.</p>
                    </ListGroup.Item>
                    <ListGroup.Item as="li" className="mt-3 d-flex align-items-start">
                      <TwitterIcon className="mr-3" width="20px" color="#FD5545" />
                      <p>Orci varius natoque penatibus et magnis dis parturient montes, ridiculus mus.</p>
                    </ListGroup.Item>
                  </ListGroup>
                </Col>
                <Col className="col-sm-6 col-lg-3 mt-4 mt-sm-0">
                  <h6 className="text-uppercase text-secondary mb-4">INSTAGRAM</h6>
                  <ListGroup variant="border-0 flex-row flex-wrap">
                    <ListGroup.Item as="a" className="col-4">
                      <Image src={insta_placeholder_01} thumbnail className="p-2" />
                    </ListGroup.Item>
                    <ListGroup.Item as="a" className="col-4">
                      <Image src={insta_placeholder_02} thumbnail className="p-2"  />
                    </ListGroup.Item>
                    <ListGroup.Item as="a" className="col-4">
                      <Image src={insta_placeholder_03} thumbnail className="p-2"  />
                    </ListGroup.Item>
                    <ListGroup.Item as="a" className="col-4">
                      <Image src={insta_placeholder_01} thumbnail className="p-2"  />
                    </ListGroup.Item>
                    <ListGroup.Item as="a" className="col-4">
                      <Image src={insta_placeholder_02} thumbnail className="p-2"  />
                    </ListGroup.Item>
                    <ListGroup.Item as="a" className="col-4">
                      <Image src={insta_placeholder_03} thumbnail className="p-2"  />
                    </ListGroup.Item>
                  </ListGroup>
                </Col>
                <Col className="col-sm-6 col-lg-3 mt-4 mt-sm-0">
                  <h6 className="text-uppercase text-secondary mb-4">NEWSLETTER</h6>
                  <Form>
                    <FormControl type="text" placeholder="You're email" size="lg" />
                    <Button variant="primary mt-3 w-100 font-weight-bold" size="lg">Subscribe</Button>
                  </Form>
                </Col>
            </Row>
            <hr/>
            <Row className="flex-column flex-sm-row flex-column-reverse">
                <Col className="text-center text-sm-left mt-3 mt-sm-0">
                    <span className="text-muted">© Copyright 2018. All Rights Reserved.</span>
                </Col>
                <Col className="text-center text-sm-right">
                <ListGroup variant="border-0 flex-row justify-content-center justify-content-sm-end">
                    <ListGroup.Item as="a" className="mr-3">
                      <FacebookIcon width="20px" color="#FD5545" />
                    </ListGroup.Item>
                    <ListGroup.Item as="a" className="mr-3">
                      <TwitterIcon width="20px" color="#FD5545" />
                    </ListGroup.Item>
                    <ListGroup.Item as="a" className="mr-3">
                      <InstagramIcon width="20px" color="#FD5545" />
                    </ListGroup.Item>
                    <ListGroup.Item as="a">
                      <YoutubeIcon width="20px" color="#FD5545" />
                    </ListGroup.Item>
                  </ListGroup>
                </Col>
            </Row>
        </Container>
      </footer>
    </>
  );
}

export default Footer;