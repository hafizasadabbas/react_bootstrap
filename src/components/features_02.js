import React from 'react';
import {Container, Row, Col, Button} from 'react-bootstrap';
import {IconBox} from '../elements';
import { Heart, Search, Star, X } from 'react-bootstrap-icons';

const Features = () => {
  var description = 'Aliquam non elit lacus. Praesent aliquet, ipsum id scelerisque convallis, mi ligula euismod.';
  return (
    <section className="py-5">
        <Container>
            <h2 className="text-center">Best services save the world</h2>
            <p className="text-center">Nam gravida, nisl a ullamcorper varius, tellus enim lobortis lectus, sit amet hendrerit neque orci quis ante. Aenean quam ligula, viverra sed luctus id, laoreet id odio. Pellentesque feugiat maximus placerat.</p>
            <Row className="flex-column flex-sm-row mt-5">
                <Col className="col-sm-6 col-lg-3 mt-4">
                    <IconBox icon={<Heart className="text-primary" />} title="Business" description={description} />
                </Col>
                <Col className="col-sm-6 col-lg-3 mt-4">
                    <IconBox icon={<Search className="text-primary" />} title="Design" description={description} />
                </Col>
                <Col className="col-sm-6 col-lg-3 mt-4">
                    <IconBox icon={<Star className="text-primary" />} title="Science" description={description} />
                </Col>
                <Col className="col-sm-6 col-lg-3 mt-4">
                    <IconBox icon={<X className="text-primary" />} title="Real Life" description={description} />
                </Col>
            </Row>
            <Row className="mt-5">
                <Col className="text-center">
                    <Button variant="primary font-weight-bold" size="lg">Read More</Button>
                </Col>
            </Row>
        </Container>
    </section>
  );
}

export default Features;