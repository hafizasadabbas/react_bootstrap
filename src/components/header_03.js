import React from 'react';
import {Container, Row, Col, Nav, Navbar, Button} from 'react-bootstrap';
import { Search } from 'react-bootstrap-icons';
import { Logo } from '../elements';

const Header = () => {
  return (
    <>
      <header className="header vh-100 half-background-shadowed">
        <div className="w-100 bg-white shadow-sm">
            <Col>
                <Container>
                    <Navbar expand="xl" className="px-0">
                        <Navbar.Brand href="#home"><Logo /></Navbar.Brand>
                        <Navbar.Toggle aria-controls="basic-navbar-nav" />
                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="m-auto">
                                <Nav.Link href="#about" className="px-3">About</Nav.Link>
                                <Nav.Link href="#portfolio" className="px-3">Portfolio</Nav.Link>
                                <Nav.Link href="#careers" className="px-3">Careers</Nav.Link>
                                <Nav.Link href="#blog" className="px-3">Blog</Nav.Link>
                            </Nav>
                            <Button variant="outline-primary font-weight-bold btn-ghost ml-3">Contact Us</Button>
                            <Search className="ml-3 text-primary"/>
                        </Navbar.Collapse>
                    </Navbar>
                </Container>
            </Col>
        </div>
        <Container>
            <Row className="align-items-center">
                <Col className="col-xl-7">
                    <div className="full-background-caption">
                        <h6 className="text-uppercase mb-3">Voluptate Velit</h6>
                        <h1 className="mb-3">
                            Best Way to Grown Your Business
                        </h1>
                        <p className="text-justify mb-3 mt-3 d-inline-block col-10 px-0">
                            Nam gravida, nisl a ullamcorper varius, tellus enim lobortis lectus, sit amet hendrerit neque orci quis ante. Aenean quam ligula, viverra sed luctus id, laoreet id odio.
                        </p>
                        <Button variant="primary font-weight-bold" size="lg">Read More</Button>
                    </div>
                </Col>
            </Row>
        </Container>
      </header>
    </>
  );
}

export default Header;