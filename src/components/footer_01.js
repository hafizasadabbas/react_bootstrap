import React from 'react';
import {Container, Row, Col, Nav,} from 'react-bootstrap';

const Footer = () => {
  return (
    <>
      <footer className="footer border-top">
          
        <Container className="py-4">
            <Row className="flex-column flex-md-row flex-column-reverse">
                <Col className="d-flex align-items-center justify-content-center justify-content-md-start">
                    <span className="text-muted">© Copyright 2018. All Rights Reserved.</span>
                </Col>
                <Col>
                    <Nav className="justify-content-center justify-content-md-end" activeKey="/home">
                        <Nav.Item>
                            <Nav.Link href="/terms" className="text-secondary">Terms and conditions</Nav.Link>
                        </Nav.Item>
                        <Nav.Item>
                            <Nav.Link href="/privacy" className="text-secondary pr-md-0">Privacy policy</Nav.Link>
                        </Nav.Item>
                    </Nav>
                </Col>
            </Row>
        </Container>
      </footer>
    </>
  );
}

export default Footer;