import React from 'react';
import {Container, Row, Col} from 'react-bootstrap';
import {IconBox} from '../elements';
import { Heart, Search, Star } from 'react-bootstrap-icons';

const Features = () => {
    var description = 'Aliquam non elit lacus. Praesent aliquet, ipsum id scelerisque convallis, mi ligula euismod.';
  return (
    <section className="py-5">
        <Container>
            <h6 className="text-center text-uppercase mb-3">WHAT WE DO</h6>
            <h2 className="text-center">Future advantages of strategic thinking and planning</h2>
            <Row className="flex-column flex-sm-row mt-5">
                <Col className="col-sm-6 col-lg-4 mt-4">
                    <IconBox icon={<Heart className="text-primary" />} title="Business" learnMore="#" description={description} />
                </Col>
                <Col className="col-sm-6 col-lg-4 mt-4">
                    <IconBox icon={<Search className="text-primary" />} title="Design" learnMore="#" description={description} />
                </Col>
                <Col className="col-sm-6 col-lg-4 mt-4">
                    <IconBox icon={<Star className="text-primary" />} title="Science" learnMore="#" description={description} />
                </Col>
            </Row>
        </Container>
    </section>
  );
}

export default Features;